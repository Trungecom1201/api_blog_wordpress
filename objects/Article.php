<?php
/**
 *contains properties and methods for "article" database queries.
 */

class Article
{

    //Db connection and table
    private $conn;
    private $table_name = 'article';

    //Object properties
    public $id;
    public $title;
    public $description;
    public $publish_date;
    public $author_name;


    //Constructor with db conn
    public function __construct($db)
    {
        $this->conn = $db;
    }


    //Read article
    function read(){

        //select all
        $query = "SELECT
                    *
                  FROM
                  " . $this->table_name . " 
                  ";

        //prepare
        $stmt = $this->conn->prepare($query);

        //execute
        $stmt->execute();

        return $stmt;

    }
// create article
    function create(){

        //query insert
        $query = "INSERT INTO
              ". $this->table_name ."
              SET
                title=:title, description=:description, publish_date=:publish_date, author_name=:author_name";

        //Prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->author_name=htmlspecialchars(strip_tags($this->author_name));
        $this->publish_date=htmlspecialchars(strip_tags($this->publish_date));

        //Bind values
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":author_name", $this->author_name);
        $stmt->bindParam(":publish_date", $this->publish_date);
        $array = array(
            "title" => $this->title,
            "description" => $this->description,
            "author_name" => $this->author_name,
            "publish_date" => $this->publish_date,
        );
        //execute
        if($stmt->execute($array)){
            return true;
        }
        return false;
    }
    //read single article
    function readOne(){

        //read single record
        $query = "SELECT
                *
            FROM
                " . $this->table_name . "  
                   WHERE
                   p.id = ? LIMIT 0,1";

        //prepare
        $stmt = $this->conn->prepare($query);

        //bind id of article
        $stmt->bindParam(1, $this->id);

        //execute
        $stmt->execute();

        //fetch row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        //set values to update
        $this->name=$row['name'];
        $this->price=$row['price'];
        $this->description=$row['description'];
        $this->category_id=$row['category_id'];
        $this->category_name=$row['category_name'];

    }



    //update article
    function update(){

        //update query
        $query = "UPDATE
                    " . $this->table_name. "
                    SET
                        title=:title,
                        description=:description,
                        author_name=:author_name
                    WHERE
                        id=:id";

        //prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->author_name=htmlspecialchars(strip_tags($this->author_name));
        $this->id=htmlspecialchars(strip_tags($this->id));

        //bind new values
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':author_name', $this->author_name);
        $stmt->bindParam(':id', $this->id);

        //execute
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    //delete article
    function delete(){

        //delete query
        $query = " DELETE FROM " . $this->table_name . " WHERE id = ?";

        //prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));

        //bind id
        $stmt->bindParam(1, $this->id);

        //execute
        if($stmt->execute()){
            return true;
        }

        return false;
    }

}
