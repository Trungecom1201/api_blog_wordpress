<?php
/**
 *contains properties and methods for "product" database queries.
 */

class Tags
{

    //Db connection and table
    private $conn;
    private $table_name = 'tags';

    //Object properties
    public $id;
    public $name;


    //Constructor with db conn
    public function __construct($db)
    {
        $this->conn = $db;
    }
    function read(){

        //select all
        $query = "SELECT
                    *
                  FROM
                  " . $this->table_name . " 
                  ";

        //prepare
        $stmt = $this->conn->prepare($query);

        //execute
        $stmt->execute();

        return $stmt;

    }
// create tags
    function create(){

        //query insert
        $query = "INSERT INTO
              ". $this->table_name ."
              SET
                name=:name";

        //Prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));


        //Bind values
        $stmt->bindParam(":name", $this->name);


        //execute
        if($stmt->execute()){
            return true;
        }
        return false;
    }


    //update tags
    function update(){

        //update query
        $query = "UPDATE
                    " . $this->table_name. "
                    SET
                        name=:name
                    WHERE
                        id=:id";

        //prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->id=htmlspecialchars(strip_tags($this->id));

        //bind new values
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':id', $this->id);

        //execute
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    //delete tags
    function delete(){

        //delete query
        $query = " DELETE FROM " . $this->table_name . " WHERE id = ?";

        //prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));

        //bind id
        $stmt->bindParam(1, $this->id);

        //execute
        if($stmt->execute()){
            return true;
        }

        return false;
    }

}
