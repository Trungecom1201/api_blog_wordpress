<?php
/**
 *contains properties and methods for "product" database queries.
 */

class Related
{

    //Db connection and table
    private $conn;
    private $table_name = 'related';

    //Object properties
    public $id;
    public $id_article;
    public $id_tag;


    //Constructor with db conn
    public function __construct($db)
    {
        $this->conn = $db;
    }
    function read(){

        //select all
        $query = "SELECT * FROM " . $this->table_name . " WHERE id_article = ?";

        //prepare
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        //execute
        $stmt->execute();

        return $stmt;

    }
// create related
    function create(){

        //query insert
        $query = "INSERT INTO
              ". $this->table_name ."
              SET
                id_article=:id_article,id_tag=:id_tag";

        //Prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->id_article=htmlspecialchars(strip_tags($this->id_article));
        $this->id_tag=htmlspecialchars(strip_tags($this->id_tag));


        //Bind values
        $stmt->bindParam(":id_article", $this->id_article);
        $stmt->bindParam(":id_tag", $this->id_tag);


        //execute
        if($stmt->execute()){
            return true;
        }
        return false;
    }
    function delete(){

        //delete query
        $query = " DELETE FROM " . $this->table_name . " WHERE id_article = ?";

        //prepare
        $stmt = $this->conn->prepare($query);

        //sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));

        //bind id
        $stmt->bindParam(1, $this->id);

        //execute
        if($stmt->execute()){
            return true;
        }

        return false;
    }
}
