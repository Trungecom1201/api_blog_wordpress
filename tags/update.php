<?php


//Req headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//Req includes
include_once '../config/database.php';
include_once '../objects/Tags.php';

//Db conn and instances
$database = new Database();
$db=$database->getConnection();

$tags = new Tags($db);

//Get post data
$data = json_decode(file_get_contents("php://input"));

//set Id and values of product to be edited
$tags->id            = $data->id;
$tags->name          = $data->name;


//update product
if($tags->update()){
    echo '{';
    echo '"message": "Tag was updated."';
    echo '}';
}else{
    echo '{';
    echo '"message": "Unable to update tag."';
    echo '}';
}
