<?php

//Required headers

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//Include db and object

include_once '../config/database.php';
include_once '../objects/Tags.php';

//New instances

$database = new Database();
$db = $database->getConnection();

$article = new Tags($db);

//Query products
$stmt = $article->read();
$num = $stmt->rowCount();

//Check if more than 0 record found
if($num > 0){

    //products array
    $articles_arr = array();
    $articles_arr["records"] = array();

    //retrieve table content
    // Difference fetch() vs fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $article_item = array(
            "id"            =>  $id,
            "name"          =>  $name,
        );

        array_push($articles_arr["records"], $article_item);
    }

    echo json_encode($articles_arr);
}else{
    echo json_encode(
        array("messege" => "No article found.")
    );
}
