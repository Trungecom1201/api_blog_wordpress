<?php

//Req headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: access");

//Include db and object

include_once '../config/database.php';
include_once '../objects/Article.php';

//New instances

$database = new Database();
$db = $database->getConnection();

$article = new Article($db);

//Set ID of product to be edited
$article->id = isset($_GET['id']) ? $_GET['id']: die;

//Read details of edited product
$article->readOne();

//Create array
$article_arr = array(
    "id" => $article->id,
    "name" => $article->name
);

print_r(json_encode($article_arr));
