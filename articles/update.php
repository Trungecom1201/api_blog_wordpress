<?php


//Req headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//Req includes
include_once '../config/database.php';
include_once '../objects/Article.php';
include_once '../objects/Related.php';
//Db conn and instances
$database = new Database();
$db=$database->getConnection();

$article = new Article($db);
$related = new Related($db);
//Get post data
$data = json_decode(file_get_contents("php://input"));

//set Id and values of article to be edited
$article->id            = $data->id;
$related->id            = $data->id;
$article->title         = $data->title;
$article->description   = $data->description;
$article->author_name   = $data->author_name;

//update article

if($article->update()){
    if($related->delete()){
        $id =  $data->id;
        $tag = explode(",",$data->tag_id);
        foreach($tag as $item ){
            $related->id_article = $id;
            $related->id_tag = $item;
            $related->create();
        }
    }
    echo '{';
    echo '"message": "Article was updated."';
    echo '}';
}else{
    echo '{';
    echo '"message": "Unable to update article."';
    echo '}';
}
