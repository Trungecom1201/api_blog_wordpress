<?php

//Required headers

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//Include db and object

include_once '../config/database.php';
include_once '../objects/Article.php';
include_once '../objects/Related.php';
//New instances

$database = new Database();
$db = $database->getConnection();

$article = new Article($db);

//Query article
$stmt = $article->read();
$num = $stmt->rowCount();

//Check if more than 0 record found
if($num > 0){

    //article array
    $articles_arr = array();
    $articles_arr["records"] = array();

    //retrieve table content
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        $related = new Related($db);
        $related->id = $id;
        $related->read();
        $related_tag = array();
        $related_all = $related->read()->fetchAll(PDO::FETCH_ASSOC);
        foreach ($related_all as $item) {
            array_push($related_tag, $item['id_tag']);
        }
        $article_item = array(
            "id"            =>  $id,
            "title"          =>  $title,
            "description"   =>  html_entity_decode($description),
            "publish_date"         =>  $publish_date,
            "author_name"   =>  $author_name,
            "tags"=>implode(",",$related_tag)
        );

        array_push($articles_arr["records"], $article_item);

    }

    echo json_encode($articles_arr);
}else{
    echo json_encode(
        array("messege" => "No article found.")
    );
}
