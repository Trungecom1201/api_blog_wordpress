<?php

//Req headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset:UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//Req includes
include_once '../config/database.php';
include_once '../objects/Article.php';
include_once '../objects/Related.php';
//Db conn and instances
$database = new Database();
$db=$database->getConnection();

$article = new Article($db);

//Get post data
$data = json_decode(file_get_contents("php://input"));

//set article values
$article->title          = $data->title;
$article->description   = $data->description;
$article->publish_date       = date('Y-m-d H:i:s');
$article->author_name   = $data->author_name;

//Create article
if($article->create()){
    $id = $db->lastInsertId();
    $tag = explode(",",$data->tag_id);
    foreach($tag as $item ){
        $related = new Related($db);
        $related->id_article = $id;
        $related->id_tag = $item;
        $related->create();
    }
    echo '{';
        echo '"message": "Article was created."';
    echo '}';
}else{
    echo '{';
        echo '"message": "Unable to create article."';
    echo '}';
}


